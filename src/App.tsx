import './App.css'
import useApi from './hooks/useApi';
type Data = {
  title: string;
};


function App() {

  const { data, refetch, loading } = useApi<Data>({
    url: `https://jsonplaceholder.typicode.com/posts/1`,
    onSuccess: (data) => {
      console.log(data);

    },
    onFailed: (error) => {
      console.log(error);

    },
  });

  return (
    <>
      <div>{loading ? "is loading" : data?.title ?? ""}</div>
      <div className="card">
        <button onClick={refetch}>
          refetch API
        </button>
      </div>

    </>
  )
}

export default App
