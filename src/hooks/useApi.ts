import { useState, useEffect } from 'react';
import axios, { AxiosResponse } from 'axios';

type HttpMethod = 'get' | 'post' | 'put' | 'delete';

interface ApiResponse<T> {
    data: T | null;
    loading: boolean;
    error: unknown;
    refetch: () => void;
}

interface RequestOptions<T> {
    url: string;
    method?: HttpMethod;
    body?: unknown;
    onSuccess?: (data: T) => void;
    onFailed?: (error: unknown) => void;
    isEnabled?: boolean;
}

const useApi = <T>({
    url,
    method = 'get',
    body,
    onSuccess,
    onFailed,
    isEnabled = true
}: RequestOptions<T>): ApiResponse<T> => {

    const [data, setData] = useState<T | null>(null);
    const [loading, setLoading] = useState<boolean>(true);
    const [error, setError] = useState<unknown>(null);

    const fetchData = async () => {
        setLoading(true);
        setError(null);

        try {
            let response: AxiosResponse<T>;
            switch (method.toLowerCase()) {
                case 'post':
                    response = await axios.post<T>(url, body);
                    break;
                case 'put':
                    response = await axios.put<T>(url, body);
                    break;
                case 'delete':
                    response = await axios.delete<T>(url);
                    break;
                default:
                    response = await axios.get<T>(url);
            }

            setData(response.data);
            onSuccess?.(response.data);
            setLoading(false);
            // Cache the data
            localStorage.setItem(url, JSON.stringify(response.data));
        } catch (error) {
            setError(error);
            onFailed?.(error)
            setLoading(false);
        }
    };

    const refetch = () => {
        fetchData();
    };

    useEffect(() => {
        if (!isEnabled) return;

        // Check if data is cached
        const cachedData = localStorage.getItem(url);
        if (cachedData) {
            setData(JSON.parse(cachedData));
            setLoading(false);
        } else {
            fetchData();
        }
    }, [url, method, body, isEnabled]);

    return { data, loading, error, refetch };
};

export default useApi;
